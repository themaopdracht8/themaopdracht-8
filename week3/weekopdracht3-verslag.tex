% !TeX root = RJwrapper.tex
\title{Weekopdracht 3 verslag}
\author{by Kim Chau Duong, Wouter Schuiten}

\maketitle


\subsection{Introductie}\label{introductie}

Corticosteroiden zijn hormonen die aangemaakt worden in de
bijnierschors. Dezelfde naam wordt gebruikt voor medicijnen die de
werking van deze hormonen nabootsen, zoals methylprednisolon (MPL).
Specifiek glucocorticoiden (GC's) worden gebruikt als medicijn bij bv
asthma. GC's binden aan de glucocorticoide receptor (GR) in de cel. De
Receptor-medicijn complex dat ontstaat zorgt voor een upregulatie van
factoren die ontsteking vanuit het immuunsysteem tegengaan, en een
downregulatie van factoren die ontstekingen vanuit het immuunsysteem
veroorzaken. Corticosteroiden worden daarom vaak gebruikt bij ziektes
die veroorzaakt worden door een overreactie van het immuunsysteem, zoals
asthma, MS en de ziekte van Crohn. \citep{cortico}

Voor de interactie tussen glucocorticoide en receptor is er een model
gedefinieerd. Het gebruik van het model resulteert in simulatie data.
Modellen kunnen in enkele gevallen zeer verschillen met de
werkelijkheid. Daarom wordt er in dit onderzoek gekeken of het MPL-GR
model overeenkomt met werkelijkheid, door het te vergelijken met
werkelijke experimentele data.

\newpage

\subsection{Materiaal en Methode}\label{materiaal-en-methode}

Experiment data werdt verzameld uit ratten, wat vrij representatief is
voor de simulatie. In het experiment werden bij ratten, in groepen van
vier, 7 dagen lang een constant infuus toegediend, met een dosis van 0.1
of 0.3 mg MPL/kg rat/uur (39 nmol/L en 107 nmol/L). Bij vaste
tijdseenheden werden in de ratten de concentratie medicijn, de receptor
mRNA en de vrije receptor in de cel gemeten. De data is grotendeels in
orde. Over het algemeen is er gelijke data voor de 0.1 dosis groep en
0.3 dosis groep aanwezig, met enkele ontbrekende data (NA values). Wel
zijn er weinig metingen per groep per tijdstip (4). Voor gebruik van de
data wordt voor de concentratie per dosis de mediaan gebruikt. Er wordt
met de mediaan gebruikt in plaats van de gemiddelde van de data, omdat
veel spreiding in de subdata is, vaak vanwege uitschieters. Door een
mediaan te gebruiken wordt er een stabielere beeld van de data
verkregen.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.85\textwidth]{images/modelf.png}
  \caption{differentiaal vergelijken in glucocorticoide receptor model}
  \label{fig:modelf}
\end{figure}

\textbf{\emph{Het Model}}\\
Het model voor glucocorticoide receptor dynamica bestaat uit meerdere
differentiaalvergelijkingen, te zien in figuur \ref{fig:modelf}. In de
cellen vormt receptor mRNA (aangegeven als mRNAR) de glucocorticoide
receptor R. Het medicijn D (MPL) bindt zich in het cytosol aan vrije R,
waardoor de DR complex vormt. Het complex wordt vervoerd naar de celkern
(DR(N)), waarbij het voor een downregulatie van de mRNAR aanmaak zorgt.
Vrije R kan gerecycled worden uit the DR(N). Het model bevat constantes.
Ks\_RM en kd\_RM zijn de snelheidsconstanten voor respectievelijk de
mRNAR aanmaak en afbraak. Ks\_R en kd\_R zijn de snelheidsconstanten
voor respectievelijk de receptor aanmaak en afbraak. Kon is de
snelheidsconstante voor de vorming van de DR complex. KT is de
snelheidsconstante voor de translocatie van DR naar de celkern (DR(N)).
Kre is de `recovery' van vrije receptor vanuit de celkern naar het
cytosol. Rf is de fractie vrije receptor dat gerecycled kan worden uit
DR(N). IC50\_Rm is de concentratie DR(N) dat nodig is om de aanmaak van
mRNA te dalen tot 50\% van de basiswaarde.\\
Voor het simuleren van het model wordt gebruik gemaakt van de deSolve
library in programmeertaal R.De waarden van de simulatie zullen in
grafieken worden vergeleken met de waarden verkregen uit de ratten
experiment.

\textbf{\emph{Andere Scenario's}}\\
Nadat de simulatie vergeleken is met de werkelijkheid, worden ook een
aantal scenario's getest met de simulatie. Er wordt onderzocht hoe de
simulatie loopt als er geen regulatie is tussen DR(N) en de aanmaak van
het mRNA. Ook testen wij wat er gebeurd als de hoeveelheid medicijn D op
0 wordt gezet op het moment dat equilibrium is bereikt. Verder worden er
verschillende waarden getest voor kon, kre, ks\_rm en kd\_rm en wordt er
gekeken naar wat er gebeurd als receptor R niet meer door het mRNA wordt
aangemaakt.

\newpage

\subsection{Resultaten}\label{resultaten}

Het model is gesimuleerd met een dosis van 39 nmol/L en 107 nmol/L MPL.
Hierin wordt een beeld gevormd van in hoeverre mate de receptor mRNA en
vrije receptor R concentraties veranderen bij toevoeging van medicijn D,
en wanneer een equilibrium bereikt wordt waarbij de aanmaak van mRNA,
vrije receptor R en gebonden receptor gelijk zal zijn. De simulatie werd
vergeleken met de werkelijke data van de ratten experiment. De data van
de ratten ziet er als volgt uit:\\

\begin{minipage}{\linewidth}
\captionof{table}{Mediaan van eerste paar waarden uit experiment data} \label{tab:headmed} 
\end{minipage}

\begin{Schunk}
\begin{Soutput}
#>   dose time MPL_conc   mRNA Free_receptor
#> 1  0.0    0    0.000 3.7900        292.95
#> 2  0.1    6   11.180 1.7025        124.70
#> 3  0.3    6   31.295 1.7295         97.90
#> 4  0.1   10   12.335 1.7515        157.80
#> 5  0.3   10   36.960 1.4140         69.55
#> 6  0.1   13   11.945 1.7045        152.50
\end{Soutput}
\end{Schunk}

\newpage

Om te bepalen hoe de medicijn dosis D invloed heeft op de simulatie en
hoe dat verschilt met de werkelijkheid werd het model gesimuleerd met
twee verschillende toegediende concentraties MPL, 39 nmol/L (0.1 mg
MPL/kg rat/uur) en 107 nmol/L (0.1 mg MPL/kg rat/uur). De verkregen
simulatie waarden samen met de rat experiment waarden werden samen in
een grafiek uitgezet om de overeenkomst en verschil in beeld te brengen.

In figuur \ref{fig:mRNAR} werd het verloop van de receptor mRNA
concentratie tijdens de simulatie en tijdens het experiment als grafiek
weergegeven. Bij beide simulaties onder verschillende doses is te zien
dat na de eerste toediening de concentraties hoog vallen en daarna sterk
dalen, waarna er een equilibrium vormt. Bij de lagere dosis van 0.1 mg
MPL/kg rat/uur, figuur \ref{fig:mRNAR_39} is er een minder ernstiger
daling van de concentratie en ligt de equilibirum hoger dan bij de 0.3
mg MPL/kg rat/uur dosis grafiek in figuur \ref{fig:mRNAR_107}. In de
werkelijkheid, via de experiment data met de ratten, blijkt dat deze
experiment waarden sterk verschillen in tegenstelling tot de simulatie.
In de experiment data fluctueert de mRNA concentratie per tijdseenheid.
Er vindt geen equilibrium plaats.de fluctuerende waarden komen op
sommige tijdseenheden bijna overeen, maar voor de meeste waarden wijken
ze af van de simulatie. de 0.3 mg MPL/kg rat/uur dosis simulatie in
figuur \ref{fig:mRNAR_107} komt meer overeen met de werkelijke data in
tegenstelling tot de 0.1 mg MPL/kg rat/ uur dosis simulatie in figuur
\ref{fig:mRNAR_39}. de experiment data in figuur \ref{fig:mRNAR_107}
lijkt stabieler met minder ernstiger uitschieters.

In figuur \ref{fig:R} werd het verloop van de vrije receptor R in de cel
tijdens de simulatie en tijdens het experiment als grafiek weergegeven.
Na toediening van MPL daalt R tot het een equilibrium bereikt. Een
verhoogde dosis van 0.3 mg MPL/kg rat/uur, afgebeeld in figuur
\ref{fig:R_107}, geeft een lagere equilibrium dan bij een dosis van 0.1
mg MPL/kg rat/uur, afgebeeld in figuur \ref{fig:R_39}. In tegenstelling
tot de mRNA waarden, lijken de werkelijke R waarden overeen te komen met
de simulatie waarden. op enkele uitschieters na liggen de experiment
waarden dicht bij de verkregen simulatie waarden.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/mRNAR_39.png}
  \caption{0.1 mg MPL/kg rat/uur dosis}
  \label{fig:mRNAR_39}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/mRNAR_107.png}
  \caption{0.3 mg MPL/kg rat/uur dosis}
  \label{fig:mRNAR_107}
\end{subfigure}
\caption{Verandering in receptor mRNA concentratie in de simulatie en in de werkelijkheid, bij verschillende MPL dosis toediening}
\label{fig:mRNAR}
\end{figure}

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/R_39.png}
  \caption{0.1 mg MPL/kg rat/uur dosis}
  \label{fig:R_39}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/R_107.png}
  \caption{0.3 mg MPL/kg rat/uur dosis}
  \label{fig:R_107}
\end{subfigure}
\caption{Verandering in vrije receptor concentratie in de simulatie en in de werkelijkheid bij verschillende MPL dosis toediening}
\label{fig:R}
\end{figure}

\newpage

\textbf{Andere Scenario's}

In het eerst scenario werd gekeken naar wat er gebeurt als er geen
auto-regulatie van de receptor aanmaak plaatsvindt, dus als DR(N) niet
meer de aanmaak van mRNAR inhibeert en hoe dit de concentratie DR(N)
beinvloedt. Om dit te bereiken moet het IC50\_Rm + DR\_N blok verwijdert
worden uit de formule:
\texttt{dmRNAR\ \textless{}-\ ks\_Rm\ *\ (1-DR\_N\ /\ (IC50\_Rm\ +\ DR\_N))\ -\ kd\_Rm\ *\ mRNAR}.
De nieuwe formule wordt dan:
\texttt{dmRNAR\ \textless{}-\ ks\_Rm\ -\ kd\_Rm\ *\ mRNAR}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.85\textwidth]{images/altDRN_39.png}
  \caption{MPL-receptor complex concentratie met en zonder auto-regulatie van mRNAR aanmaak, in vergelijking met de basis scenario}
  \label{fig:altDRN_39}
\end{figure}

In figuur \ref{fig:altDRN_39} is te zien dat het equilibrium voor de
MPL-receptor complex concentratie hoger zal liggen zonder auto-regulatie
van mRNAR aanmaak, omdat de totale hoeveelheid mRNAR hoger zal zijn, en
dit als gevolg zal hebben dat R, DR en DR(N) ook hoger zullen liggen.

\newpage

In het volgende scenario werd gekeken wat er gebeurd als medicijn D op 0
gezet wordt op het moment dat het eerste equilibrium behaald wordt, zie
grafiek \ref{fig:alt2_39}.Hiering is te zien dat nadat D=0 wordt zullen
alle waarden weer richting hun startwaarden stijgen of dalen. DR (figuur
\ref{fig:alt2DR_39}) en DR(N) (figuur \ref{fig:alt2DRN_39}) worden weer
0 fmol / mg protein. R (figuur \ref{fig:alt2R_39}) en mRNAR (figuur
\ref{fig:alt2mRNAR_39}) productie zal weer toenemen totdat een nieuq
equilibrium bereikt wordt.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt2mRNAR_39.png}
  \caption{mRNAR gehalte als D=0 bij T=35}
  \label{fig:alt2mRNAR_39}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt2R_39.png}
  \caption{R gehalte als D=0 bij T=35}
  \label{fig:alt2R_39}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt2DR_39.png}
  \caption{DR gehalte ald D=0 bij T=35}
  \label{fig:alt2DR_39}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt2DRN_39.png}
  \caption{DR(N) gehalte ald D=0 bij T=35}
  \label{fig:alt2DRN_39}
\end{subfigure}
\caption{Verandering in mRNAR, R, DR en DR(N) concentraties wanneer de MPL toediening stopt zodra het equilibrium heeft bereikt (T=35) (initiële toediening = 39 nmol/L MPL), in vergelijking met de basis scenario}
\label{fig:alt2_39}
\end{figure}

\newpage

Omdat niet alle corticosteroiden dezelfde mate van associatie en
dissociatie met hun receptor hebben werd er in deze scenario het model
gesimuleerd waarbij, onder een constante MPL toediening, de Kon en Kre
waarden verschillen. Er werd gesimuleerd met de normale, halve en 1/5e,
dubbele en vijf dubbele van de Kon en Kre waarden.

In figuur \ref{fig:Kon} zijn de simulatie waarden voor de mRNAR en R
concentraties weergegeven voor de verschillende Kon waarden. Te zien in
figuur \ref{fig:alt3mRNAR_Kon} is dat hoe lager de Kon wordt, hoe minder
sterk de initiele daling van mRNAR is, dat werd veroorzaakt door
toediening van MPL, en hoe hoger de mRNAR concentratie equilibrium gaat
liggen. Een verhoogde Kon waarde heeft een omgekeerde werking. Hogere
Kon waarden zullen een sterkere mRNAR concentratie daling geven, met een
lagere equilibrium. de R concentratie in figuur \ref{fig:alt3R_Kon} komt
hier ook mee overeen. Een lagere Kon waarde geeft een hogere R
concentratie equilibrium, en een hogere Kon waarde geeft een lagere R
concentratie equilibrium.\\
In figuur \ref{fig:Kre} zijn de simulatie waarden voor de mRNAR en R
concentraties weergegeven voor de verschillende Kre waarden. Te zien in
figuur \ref{fig:alt3mRNAR_Kre} is dat een verlaagde Kre waarde een
lagere mRNAR concentratie equilibrium geeft, en de snelheid waarmee de
equiblibrium bereikt wordt light vertraagt. Een verhoogde Kre waarde
geeft een hogere mRNAR concentratie equilibrium bij
\texttt{Kre\ =\ 0.57*2}, maar bij \texttt{Kre\ =\ 0.57*5} loopt de
simulatie kris kras. de equilibrium daarvan lijkt dicht bij the 0 te
zitten. In figuur \ref{fig:alt3R_Kre} is deze situatie voor de R
concentratie afgebeeld. Het komt overeen met de mRNAR concentratie
grafiek, alleen in dit figuur is te zien dat de \texttt{Kre\ =\ 057*5}
simulatie nog heftiger fluctueert.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt3mRNAR_Kon.png}
  \caption{mRNAR concentratie verandering bij 5 verschillende Kon waarden}
  \label{fig:alt3mRNAR_Kon}
\end{subfigure}\hspace{0.05\textwidth}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt3R_Kon.png}
  \caption{R concentratie verandering bij 5 verschillende Kon waarden}
  \label{fig:alt3R_Kon}
\end{subfigure}
\caption{Verandering in vrije mRNAR en R concentraties in de simulatie bij verschillende Kon waarden}
\label{fig:Kon}
\end{figure}

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt3mRNAR_Kre.png}
  \caption{mRNAR concentratie verandering bij 5 verschillende Kre waarden}
  \label{fig:alt3mRNAR_Kre}
\end{subfigure}\hspace{0.05\textwidth}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt3R_Kre.png}
  \caption{R concentratie verandering bij 5 verschillende Kre waarden}
  \label{fig:alt3R_Kre}
\end{subfigure}
\caption{Verandering in vrije mRNAR en R concentraties in de simulatie bij verschillende Kre waarden}
\label{fig:Kre}
\end{figure}

\newpage

In het volgende scenario werd onderzocht wat er gebeurt als de productie
van receptor R volledig geblokkeerd wordt. Om dit te bereiken moet
factor Ks\_r geblokkeerd worden, dus deze parameter werd op 0 gezet. De
resultaten werden afgebeeld in figuur \ref{fig:alt4} . Te zien is dat
zonder R synthese zullen de R, DR en DR(N) gehaltes dalen tot 0. er zal
dan niks meer aanwezig zijn. de mRNAR concentratie zal een hogere
equilibrium bereiken. Omdat het mRNA niet meer naar receptor R omgezet
wordt, zal R eventueel opraken. D bindt aan R en kan alleen nog maar
aangemaakt worden door de teruggave van DR(N). Dit leidt ertoe dat R, DR
en DR(N) allemaal op 0 komen en mRNA weer zal stijgen.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt4mRNAR.png}
  \caption{mRNAR gehalte}
  \label{fig:alt4mRNAR}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt4R.png}
  \caption{R gehalte}
  \label{fig:alt4R}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt4DR.png}
  \caption{DR gehalte}
  \label{fig:alt4DR}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt4DRN.png}
  \caption{DR(N) gehalte}
  \label{fig:alt4DRN}
\end{subfigure}
\caption{Verandering in mRNAR, R, DR en DR(N) concentraties wanneer de R synthese volledig geblokkeerd wordt (Ks\_r = 0), in vergelijking met de basis scenario}
\label{fig:alt4}
\end{figure}

\newpage

In het laatste scenario werd gekeken naar wat er gebeurd als ks\_Rm
verandert. Omdat ks\_rm gekoppeld is aan kd\_rm, zullen beide variabelen
veranderen. In totaal werden 4 situaties getest:

\begin{verbatim}
ks_Rm = 2.9/5 and kd_Rm=2.9/5/4.74
ks_Rm = 2.9/2 and kd_Rm=2.9/2/4.74
ks_Rm = 2.9x2 and kd_Rm=2.9x2/4.74
ks_Rm = 2.9x5 and kd_Rm=2.9x5/4.74
\end{verbatim}

Zie ook figuur \ref{fig:alt5} en \ref{fig:alt5295}. De enige grafiek
waarbij de Ks\_Rm verandering veel invloed leek te hebben is de mRNAR
concentratie grafiek. In de andere grafieken komen de waarden sterk met
elkaar overeen.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5mRNAR.png}
  \caption{mRNAR gehalte}
  \label{fig:alt5mRNAR}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5R.png}
  \caption{R gehalte}
  \label{fig:alt5R}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5DR.png}
  \caption{DR gehalte}
  \label{fig:alt5DR}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5DRN.png}
  \caption{DR(N) gehalte}
  \label{fig:alt5DRN}
\end{subfigure}
\caption{Verandering in mRNAR, R, DR en DR(N) concentraties bij veranderingen in de ks\_Rm, en dus ook de kd\_rm}
\label{fig:alt5}
\end{figure}

\newpage

De \texttt{ks\_Rm\ =\ 2.90*5} waarden werden in aparte grafieken
uitgezet, omdat deze waarden te ver uitweken van de rest.

\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5295mRNAR.png}
  \caption{mRNAR gehalte}
  \label{fig:alt5295mRNAR}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5295R.png}
  \caption{R gehalte}
  \label{fig:alt5295R}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5295DR.png}
  \caption{DR gehalte}
  \label{fig:alt5295DR}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/alt5295DRN.png}
  \caption{DR(N) gehalte}
  \label{fig:alt5295DRN}
\end{subfigure}
\caption{Verandering in mRNAR, R, DR en DR(N) concentraties bij ```ks\_Rm = 2.90*5 en kd\_Rm = 2.90*5/4.74```}
\label{fig:alt5295}
\end{figure}

\newpage

\subsection{Conclusie}\label{conclusie}

De originele vraag van deze oefening, is of de uitgewerkte simulatie
overeenkomt met data uit een werkelijk experiment. Dit lijkt niet waar
te zijn of deels waar. De werkelijke hoeveelheid R receptor geeft
voldoende overlap om te zeggen dat deze overeenkomt met de simulatie,
echter komt de overlap in de hoeveelheid mRNA onvoldoende overeen om te
concluderen dat de simulatie correct is.

\subsection{Discussie}\label{discussie}

\subsubsection{Hoofdvraag}\label{hoofdvraag}

De data van de simulatie begint overeen te komen voor de mRNA waarden,
maar tussen T = 25 en T = 168 is er duidelijk geen equilibrium in de
werkelijke situatie bereikt. De muizen laten lagere waardes zien dan de
simulatie om vervolgens tegen het einde van het experiment weer meer
mRNA aan te maken.

Een verklaring zou zijn dat we de mediaan gebruiken, terwijl er in
werkelijkheid meerdere samples zijn elk met hun eigen startwaardes. Dit
zorgt natuurlijk voor de nodige veranderingen in hoe elk sample reageert
op het medicijn. Tevens bevat de data weinig samples per tijd stip en
worden N/A values ook verwijderd, waardoor uitschieters significant de
resultaten kunnen beinvloeden. Zo is er bij T = 168 en dose = 0.1 maar 1
sample die geen N/A value heeft. Dit zal waarschijnlijk de meest
aanneembare reden zijn dat de data niet goed te vergelijken is.

Wanneer een grafiek voor de hoeveelheid vrije R receptor uitgezet wordt,
blijkt dat deze overeenkomt met de simulatie. Omdat de R receptor wel
overeenkomt, maar het mRNA niet, lijkt het alsof in de werkelijkheid de
aanmaak/inhibitie van het mRNA anders verloopt. In beiden situaties
lijken de eerste tijdstippen overeen te komen met de simulatie, maar
zodra DR(N) invloed begint uit te oefenen, begint de situatie te
veranderen.

Om meer overeen te komen met de simulatie, zou het langer moeten duren
voordat de hoeveelheid mRNA in equilibrium komt. Het wijzigen van de
variabelen heeft hier weinig impact op, omdat alleen R significante
impact heeft op het bereiken van equilibrium, en het wijzigen van R de
beginsituatie van de simulatie weer teveel doet afwijken. Het is
mogelijk dat het daarom door een van de parameters komt. We testen de
simulatie door de parameters te wijzigen. Na het verdubbelen en halveren
van elke parameter is er geen duidelijke overeenkomst te vinden.

Tevens komt in de werkelijke data het mRNA en de hoeveelheid vrije R
receptor niet tot equilibrium, het is daarom mogelijk dat in de
werkelijkheid er andere factoren invloed uitoefenen op de variabelen.

\subsubsection{Andere Scenario's}\label{andere-scenarios}

Bij de eerste scenario werd gekeken wat er zou gebeuren als er geen
auto-regulatie van de mRNAR zou zijn. Zonder auto-regulatie stijgt het
equilibrium van de DR(N) gehalte. Dit komt waarschijnlijk doordat een
afwezigheid van de auto-regulatie er voor zal zorgen dat mRNAR ongeremd
wordt aangemaakt. Dit zorgt voor meer R, waardoor meer DR complexen
kunnen worden gevormd, en dus meer DR naar de celkern kunnen worden
getransloceerd.

Bij de tweede scenario werd gekeken wat er zou gebeuren als de MPL
toediening zou stoppen wanneer de eerste equilibriumpunt werd bereikt.
Dit gebeurde bij T=35. na T=35 werd D = 0. Hierdoor stegen de mRNAR en R
gehaltes, terwijl de DR en DR(N) gehaltes daalden. Dit is te verklaren
doordat een gebrek aan D zal leiden tot een gebrek aan DR complex
aanmaak, dus minder DR dat naar de celkern wordt getransloceerd. Een
gebrek aan DR(N) leidt tot minder negatieve feedback voor de mRNAR
aanmaak, waardoor er meer mRNAR aangemaakt wordt, en dus ook meer R.

Bij de derde scenario werd gekeken wat er zou gebeuren als de Kon en Kre
waarden werden aangepast. een lager Kon zorgt voor hogere mRNAR en R
equilibrium en lagere DR en DR(N) equilibrium. Kon is de
snelheidsconstante voor MPL-receptor associatie. dus minder Kon zal voor
minder DR en DR(N) complexen zorgen, dus mee mRNAR aanmaak en vrije R in
het cytosol. Kre is de snelheidsconstante voor de dissociatie van de DR
complex tot vrije receptors. Bij minder Kre zal DR en DR(N) hoog
blijven, en mRNAR en R dus dalen. Dit alles werd afgebeeld in figuur
\ref{fig:alt2_39}. De enige waarde wat zeer afwijkt van de verwachting
is the \texttt{Kre\ =\ 0.57*5} waarde. een hoge Kre waarde zou betekenen
dat er meer DR(N) omgezet zou worden tot R, en dus de R concentratie zou
stijgen. De simulatiewaarden geven aan dat er een eigenaardige
fluctuatie plaatsvindt. Hier hebben wij geen verklaring voor.

Bij de vierde scenario werd gekeken wat er zou gebeuren als de synthese
van vrije receptor R volledig werd geblokkeerd, zie figuur
\ref{fig:alt4}. Bij een blokkade van de synthese zal er geen R meer
aangemaakt worden via de mRNAR, en R zal uiteindelijk opraken. de enige
bron van R aanmaak is de DR(N) tot R omzetting. Dit betekent dat de
DR(N) ook zal dalen, en daarmee dus ook de DR complex. een lage DR en
DR(N) gehalte zorgt ervoor dat mRNAR ongeremd wordt aangemaakt, maar dit
kan vanwege de blokkade geen R produceren.

Bij de laatste scenario werd gekeken wat er zou gebeuren als de ks\_Rm
en kd\_Rm wordt aangepast. Ks\_Rm is de snelheidsconstante voor mRNAR
aanmaak. Daarom is de enige grafiek waar het duidelijk invloed heeft de
mRNAR concentratie grafiek, zie figuur \ref{fig:alt5mRNAR}. de andere
concentratiewaarden worden niet echt beinvloed. Waarschijnlijk komt dat
doordat er veel andere factoren deze concentraties beinvloeden. de
simulatiewaarden bij \texttt{ks\_Rm\ =\ 2.90*5} verschillen zeer met de
rest van de simulatiewaarden. Waarom dat zo is is onduidelijk.

\subsubsection{Simulatie}\label{simulatie}

Waarom reageert de simulatie voornamelijk op de hoeveelheid R? Dit komt
omdat als er geen vrije receptoren aanwezig zijn in de beginsituatie, D
alleen kan binden aan de vrije receptoren die door het mRNA en door
DR(N) aangemaakt worden, er is dus een zwakke reactie en equilibrium is
snel bereikt. Bij een hoge R kan D continue blijven binden totdat
uiteindelijk dit ook een equilibrium bereikt, de grafiek zal er
vergelijkbaar zijn als de originele simulatie, maar de concentraties DR
en DR(N) zal hoger liggen.

Waarom reageert de start van de simulatie sterk bij geen mRNA? Dit komt
waarschijnlijk door het onderdeel kd\_Rm * mRNAR in de formule van
mRNAR, omdat mRNAR nu 0 is, zal deze enorm gaan groeien. Hij bereikt in
ongeveer T=3 weer de beginwaarde van \textasciitilde{}4, hierna loopt de
simulatie ongeveer hetzelfde als met de originele beginwaarde van 4.74

\newpage

\bibliography{rferences}

\address{%
Kim Chau Duong\\
Student of Hanze University of Applied Sciences\\
Zernikeplein 11\\
}
\href{mailto:k.c.duong@st.hanze.nl}{\nolinkurl{k.c.duong@st.hanze.nl}}

\address{%
Wouter Schuiten\\
Student of Hanze University of Applied Sciences\\
Zernikeplein 11\\
}
\href{mailto:w.j.schuiten@st.hanze.nl}{\nolinkurl{w.j.schuiten@st.hanze.nl}}

