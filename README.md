# Model van Laag en Hoog Pathogene Virus Infecties en de Rol van Macrofagen

## Themaopdracht 8 Repository for Group 3

### Kim Duong & Wouter Schuiten

This repository contains our finished assignments done in Theme 08.   
* Old assignments have been moved to their designated folder.  

### Reproduced Research  

* Our reproduced research report can be found in the main folder as 'Eindverslag.rmd', as an R Markdown file. 
* The PDF version has been produced as 'RJwrapper.pdf'.   
* The R script, containing the code used in our research can be found in 'EindopdrachtThema8Stable.rmd' and in 'EindopdrachtThema8Stable.pdf' as pdf version.
* The R script uses data from .csv files, found in the 'csvfiles' folder.
* The report .rmd file 'Eindverslag.rmd' uses images to create its figures, which can be found in the 'images' folder.
* It also uses 'RJournal.sty' as it's template.
* 'RJreferences.bib' is used as a file containing all the references used in the report.

### Versions

* R version 3.4.4 (2018-03-15)
* RStudio Desktop 1.1.453 
* Engauge Digitizer version 10.6
* deSolve version 1.21
* Pracma version 2.1.4

All software mentioned above is free and available for download on their respective website.

### How to use

* Load the EindopdrachtThema8Stable.rmd file into Rstudio.
* Ensure all packages mentioned in Versions are installed using the console (example: install.packages("deSolve")).
* Ensure the 'csvfiles' folder is present in the working directory.
* Select run all to reproduce all data and graphs.
* Reproduce a single graph by using the "run current chunk" option.
* Refer to commentary to determine which chunk produces which graph.
* For the report 'Eindverslag.rmd' file, Make sure the 'images' folder, 'RJreferences.bib' and 'RJournal.sty' are present in the working directory and press the knit button to create the RJwrapper PDF version of the report.