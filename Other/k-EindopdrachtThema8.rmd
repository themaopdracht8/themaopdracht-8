---
title: "TBD"
author: "W J Schuiten / K Duong"
date: "June 1, 2018"
output: pdf_document
---

```{r}
library(deSolve)
#Define parameters H1N1 strain
parametersH1N1 <- c(B = 8.4*10^-9, p = 4.6, k = 2.6*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, aLP = 1.8*10^-2, a = 2.2*10^-1, Pm = 2*10^-1, y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define parameters H5N1 strain
parametersH5N1 <- c(B = 6.0*10^-9, p = 4.9, k = 2.2*10^-9, u = 9.7*10^1, s = 372997, V50 = 3.4*10^-4, a = 5.0*10^-2, aHP = 4.9*10^-1, Pm = 5.4*10^-2, y = 3.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define model
influenza_sim <- function(t,state,parms){
  with(as.list(c(state,parms)),{
    dT <- -B*T*V
    dI <- B*T*V-Oi*I
    dV <- (1-E1)*(p*I+Pm*Mi)- c*V - k*A*V - B*T*V
    dMr <- s - (1-E2)*a*V*Mr/(V50+V) - Omr*Mr
    dMa <- (1-E2)*a*V*Mr/(V50+V)-y*Ma*V - Oma*Ma
    dMi <- y*Ma*V - Omi*Mi
    dA <-  u*Ma+rho*A
    list(c(dT, dI, dV, dMr, dMa, dMi, dA))
  }
  )
}

#Define variable initial state
stateH1N1 <- c(T = 7*10^9, I = 0, V = 1*10^2, Mr = 508725.2/0.04, Ma = 0, Mi = 0, A = 0)
stateH5N1 <- c(T = 7*10^9, I = 0, V = 1*10^2, Mr = 372997/0.04, Ma = 0, Mi = 0,  A = 0)

#Define time sequence
times <- seq(0, 15,  by = 0.01)

out  <- ode(times = times, y = stateH5N1, parms = parametersH5N1, func = influenza_sim)
out
plot(out, type = "l", log="y")
```

```{r}
library(deSolve)
#Define parameters H1N1 strain
parametersH1N1 <- c(B = 8.4*10^-9, p = 4.6, k = 2.6*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 1.8*10^-2, aHP = 2.2*10^-1, Pm = 2*10^-1, y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define parameters H5N1 strain
parametersH5N1 <- c(B = 6.0*10^-9, p = 4.9, k = 2.2*10^2, u = 9.7*10^1, s = 372997, V50 = 3.4*10^-4, aLP = 5.0*10^-2, aHP = 4.9*10^-1, Pm = 5.4*10^-2, y = 3.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define model
influenza_sim <- function(t,state,parms){
  with(as.list(c(state,parms)),{
    dTe <- -B*Te*V
    dI <- 1
    dV <- 1
    dMr <- 1
    dMa <- 1
    dMi <- 1
    dA <- 1
    list(c(dTe, dI, dV, dMr,dMa,dMi,dA))
  }
  )
}

#Define variable initial state
state <- c(Te = 7*10^9, I = 0 , V = 1*10^2, Mr = 508725.2/0.04, Ma = 0, Mi = 0, A = 0)
stateH5N1 <- c(Te = 7*10^9, I = 0, Mr = 372997/0.04, Ma = 0, Mi = 0, V = 1*10^2, A = 0)

#Define time sequence
times <- seq(0, 15,  by = 1)

out  <- ode(times = times, y = state, parms = parametersH1N1, func = influenza_sim, method = "euler")
out
plot(out, type = "l", ylim = c(0, 1*10^10))
```

```{r}
SSR <- ((sum(log10(out.H1N1lp[values, "V"] + out.H1N1hp[values, "V"])) 
         - sum(log10(my.viral_count_data$TX.91..H1N1.+ my.viral_count_data$X1918..H1N1.)))
         
         /log10(max.v))^ +
      ((sum(log10(out.H1N1lp[values, "Ma"]+out.H1N1lp[values, "Mi"]+
                     out.H1N1hp[values, "Ma"]+out.H1N1hp[values, "Mi"]))
      - sum(log10(my.mp_data$TX.91)))/log10(max.mp))^2

```

```{r}
#Define parameters H1N1 LP strain as simulated in article, y and Pm = 0, and using a = LP
parametersH1N1LP <- c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 1.8*10^-2, Pm = 0, y = 0, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define parameters H1N1 HP strain as simulated in article, using a = HP
parametersH1N1HP <- c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 2.2*10^-1, Pm = 2*10^-1, y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define parameters H5N1 LP strain as simulated in article, y and Pm = 0, and using a = LP
parametersH5N1LP <- c(B = 6.0*10^-9, p = 4.9, k = 2.2*10^-9, u = 9.7*10^1, s = 372997, V50 = 3.4*10^-4, a = 5.0*10^-2, Pm = 0, y = 0, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)


#Define parameters H5N1 HP strain as simulated in article, using a = HP
parametersH5N1HP <- c(B = 6.0*10^-9, p = 4.9, k = 2.2*10^-9, u = 9.7*10^1, s = 372997, V50 = 3.4*10^-4, a = 4.9*10^-1, Pm = 5.4*10^-2, y = 3.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)


```

```{r}
# Edited data during parameter fitting
#Define parameters H1N1 LP strain as simulated in article, y and Pm = 0, and using a = LP
parametersH1N1LP <- c(B = 8.5*10^-9, p = 4.6, k = 2.4*10^2, u = 2.4*10^-9, s = 508725.2*2, V50 = (3.9/2)*10^-4, a = 1.9*10^-2, Pm = 0, y = 0, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define parameters H1N1 HP strain as simulated in article, using a = HP
parametersH1N1HP <- c(B = 8.5*10^-9, p = 4.6, k = 2.4*10^2, u = 2.4*10^-9, s = 508725.2*2, V50 = (3.9/2)*10^-4, a = 2.3*10^-1, Pm = 2*10^-1, y = 2.3*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

#Define parameters H5N1 LP strain as simulated in article, y and Pm = 0, and using a = LP
parametersH5N1LP <- c(B = 6.0*10^-9, p = 4.9, k = 2.2*10^-9, u = 9.7*10^1, s = 372997, V50 = 3.4*10^-4, a = 5.0*10^-2, Pm = 0, y = 0, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)


#Define parameters H5N1 HP strain as simulated in article, using a = HP
parametersH5N1HP <- c(B = 6.0*10^-9, p = 4.9, k = 2.2*10^-9, u = 9.7*10^1, s = 372997, V50 = 3.4*10^-4, a = 4.9*10^-1, Pm = 5.4*10^-2, y = 3.3*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0)

```

```{r}
ctimes <- seq(0,100, by = 0.01)
cout.H1N1hp <- ode(times = ctimes, y = stateH1N1, parms = parametersH1N1HP, func = influenza_sim)


ccout.H1N1hp <- cumsum(cout.H1N1hp[,"V"])
#plot(ccout.H1N1hp, x = ctimes, log = "x")
head(cout.H1N1hp)
head(ccout.H1N1hp)
```

```{r}
#Define values to test for parameter
session <- c(0.01, 0.1,1,10,100)

#Define time points where real data is available
values <- c(1:151)

#range of values for the parameter to test
res.a <- vector(length(session),mode="list")
res.pm <- vector(length(session),mode="list")
res.y <- vector(length(session),mode="list")

#Execute ode for each value in session list, multiplying defined parameter by chosen value
for (k in seq_along(session)){ 
    res.a[[k]] <- ode(y=stateH1N1,times=times,func=influenza_sim,
           parms=c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 2.2*10^-1* session[k], Pm = 2*10^-1, y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0))
}

totalvirus.a <- c(sum(res.a[[1]][values, "V"]), sum(res.a[[2]][values, "V"]), sum(res.a[[3]][values, "V"]), sum(res.a[[4]][values, "V"]), sum(res.a[[5]][values, "V"]))
```

```{r}
testlist <- seq(0,10, by = 0.1)
list.a <- vector(length(testlist), mode = "list")

for(li in seq_along(testlist)){
  list.a[[li]] <- ode(y=stateH1N1,times=times,func=influenza_sim,
           parms=c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 2.2*10^-1* testlist[li], Pm = 2*10^-1, y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0))[values,"V"]
}
#head(list.a)
reslist <- vector(length(testlist),mode="list")
#clist.a <- cumsum(list.a)
#plot(clist.a, x = testlist)
for(lis in seq_along(testlist)){
  reslist[[lis]] <- sum(list.a[[lis]])
}
head(reslist)
plot(reslist, x = testlist)
```

```{r}
#Define values to test for parameter
#testlist <- c(0.01, 0.1,1,10,100)
testlist <- seq(0.01,100, by=1)

#Define time points where real data is available
values <- c(1:151)

#range of values for the parameter to test
res.a <- vector(length(testlist),mode="list")
res.pm <- vector(length(testlist),mode="list")
res.y <- vector(length(testlist),mode="list")

### maybe merge 2 for loops into 1

#alpha

for (k in seq_along(testlist)){ 
    res.a[[k]] <- ode(y=stateH1N1,times=times,func=influenza_sim,
           parms=c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 2.2*10^-1* testlist[k], Pm = 2*10^-1, y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0))[values,"V"]
}
reslist.a <- vector(length(testlist),mode="list")
for(lis in seq_along(testlist)){
  reslist.a[[lis]] <- sum(res.a[[lis]])
}

#Pm
for (k in seq_along(testlist)){ 
    res.pm[[k]] <- ode(y=stateH1N1,times=times,func=influenza_sim,
           parms=c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 2.2*10^-1, Pm = 2*10^-1* testlist[k], y = 2.2*10^-3, Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0))[values,"V"]
}
reslist.pm <- vector(length(testlist),mode="list")
for(lis in seq_along(testlist)){
  reslist.pm[[lis]] <- sum(res.pm[[lis]])
}

#y
for (k in seq_along(testlist)){ 
    res.y[[k]] <- ode(y=stateH1N1,times=times,func=influenza_sim,
           parms=c(B = 8.4*10^-9, p = 4.6, k = 2.5*10^2, u = 2.5*10^-9, s = 508725.2, V50 = 3.9*10^-4, a = 2.2*10^-1, Pm = 2*10^-1, y = 2.2*10^-3* testlist[k], Omr = 0.04, Oma = 0.04, Omi = 0.04, Oi = 2, rho = 1, c = 3, E1 = 0, E2 = 0))[values,"V"]
}
reslist.y <- vector(length(testlist),mode="list")
for(lis in seq_along(testlist)){
  reslist.y[[lis]] <- sum(res.y[[lis]])
}

```

```{r}
plot(reslist.a, x = testlist, log = "xy", ylim=c(10000, 1000000000000))
lines(reslist.pm, x= testlist, type="o", col ="blue")
lines(reslist.y, x = testlist, type="l", col = "red")
```