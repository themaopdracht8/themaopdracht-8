---
title: "Les3uitwerking"
author: "W J Schuiten / K Duong"
date: "May 8, 2018"
output: pdf_document
---


Opdracht week 2

[1] Schrijf een korte introductie over corticosteroiden, het gebruik ervan en het werkingsmechanisme.

  Corticosteroiden zijn hormonen die gemaakt worden in het bijnierschors, dezelfde naam wordt gebruikt voor medicijnen     die de werking van deze hormonen nabootsen, zoals prednisolon. Specifiek glucocorticoiden (GC's) worden gebruikt als     medicijn bij bv asthma. GC's binden aan de glucocorticoide receptor (GR) in de cel, het geactiveerde complex up          reguleert vervolgens factoren die ontsteking vanuit het immuunsysteem tegengaan, en downreguleert factoren die           ontstekingen vanuit het immuunsysteem veroorzaken.
  
  Corticosteroiden worden daarom vaak gebruikt bij ziektes die veroorzaakt worden door een overreactie van het             immuunsysteem, zoals asthma, MS en de ziekte van Crohn.   

[2] Beschrijf op basis van het model hoe de interactie verloopt tussen het geneesmiddel en de productie van receptor.

  Het geneesmiddel beinvloed op 2 manieren de productie van de hoeveelheid receptor. Het medicijn bindt aan de receptor in het cytoplasma om DR te vormen, en vervoert zich naar the celkern (DR(N)). Een deel van DR(N) wordt na gebruik weer terug omgezet naar de receptor R. DR(N) inhibeert de   aanmaak van mRNA R, omdat mRNA R omgezet wordt tot receptor R zal DR(N) ook direct de aanmaak van receptor R             inhiberen.

[3] Implementeer het model in R, waarbij je er van uit kunt gaan dat de concentratie MPL constant is (een parameter) 
en gelijk is aan 20 ng/mL. Denk er om dat de eenheid omgerekend moet worden om de vergelijkingen kloppend te maken. 
Dus 20 ng/ml * 1000 ml/L * 1 mol/ molgewicht g geeft nmol/L.

```{r}
#Logboek

#Omzetten D
# 20 ng / ml * 1000 mL / L * 1 mol / 374.471 g
# 20 * 10 ^ -9  * 10 ^ 3 / L * 1 mol / 374.471  (wegstrepen)
# 20 * 1000 / 374.417 nmol / liter = 53.41 nmol / liter

#Formule 1:
#dmRNA / dt = ks_Rm * (1 - DR(N) / IC50_rm + DR(N)) - kd_Rm * mRNAr

 # = fmole / g liver / h * (1 -  fmol/mg protein /(fmol/mg protein + fmol/mg protein)) - 1 / h * fmol / g liver
 # Eenheid = fmol / g liver / h


#Parameters 
# ks_Rm                                              2.90 
# IC50_Rm                                            26.2
# kd_Rm                                              0.612 

#Variabelen:
#     mRNAR - starting state 4.74
#     DR(N) - starting state 0
#     fmol / mg protein


# dmRNA / dt = 2.90 * (1 - 20 / 26.2 + DR(N)) - 1 / 0.612 * mRNAr



#Formule 2:
# dR / dT = ks_r * mRNAR + Rf * Kre * DR(N) - Kon * D * R - Kd_r * R

#         = g liver / mg protein / h * fmol / g liver  + 1 * 1 / h * fmol/mg protein - L/nmol/h * nmol/L * fmol/mg              protein - 1 / h * fmol/mg protein
#       eenheid = fmol / mg protein / h

#Parameters:
# ks_r                                        3.22
# Rf                                          0.49
# Kre                                         0.57
# D                                           53.41   
# Kd_r                                        0.0572 
# Kon                                         0.00329 
   
#Variabelen:
# DR(N) - starting state 0
# R -     starting state 267


#Formule 3:

#dDR / dT = kon * D * R - kt * DR

#         = L/nmol/h * nmol/L * fmol/mg protein - 1 / h * fmol/mg protein
#         Eenheid = fmol / mg protein / h

#Parameters
# Kon                                       0.00329
# D                                         53.41
# kt                                        0.63

#Variabelen
# R   starting state 267
# DR  starting state 0

#Formule 4

#dDR(N) / dT = kt * DR - kre * DR(N)

#            = 1/h * mol/mg protein - 1/h * mol/mg protein
#            Eenheid = fmol / mg protein / h

#Parameters
# kt                                     0.63
# kre                                    0.57

#Variabelen
# DR    starting state 0
# DR(N) starting state 0

```

```{r}
library(deSolve)
#Define parameters
parameters <- c(ks_r = 3.22, D = 53.41, Rf = 0.49, Kre = 0.57, kd_r = 0.0572, kon = 0.00329, kt = 0.63, ks_Rm = 2.90, IC50_Rm = 25.2, kd_Rm = 0.612) 

#Define model
MPL_receptor_sim <- function(t,state,parms){
  with(as.list(c(state,parms)),{
    dmRNAR <- ks_Rm * (1-DR_N / (IC50_Rm + DR_N)) - kd_Rm * mRNAR
    dR = ks_r * mRNAR + Rf * Kre * DR_N - kon * D * R - kd_r * R
    dDR = kon * D * R - kt * DR
    dDR_N = kt * DR - Kre * DR_N
    list(c(dmRNAR, dR, dDR, dDR_N))
    #TO DO: Complete function with the 3 other formulas, run the simulation, get it working
  }
  )
}

#Define variable initial state
state <- c(mRNAR = 4.74, R = 267, DR = 0, DR_N = 0)

#Define time sequence
times <- seq(0, 48,  by = 1)

out  <- ode(times = times, y = state, parms = parameters, func = MPL_receptor_sim, method = "euler")
out
plot(out[, "mRNAR"], type = "l", main = "Change in volume mRNA R per hour", ylab = "MRNA R (fmol / G liver )", xlab = "Time (hours)", sub = "Afbeelding 1: verandering mRNA volume per uur richting equilibrium")
plot(out[, "R"], type = "l", main = "Change in volume receptor R per hour", ylab = "R (fmol / mg protein )", xlab = "Time (Hours)", sub = "Afbeelding 2: verandering volume receptor R per uur richting equilibrium")
plot(out[, "DR"], type = "l", main = "Change in density DR per hour", ylab = "Density DR (fmol / mg protein)", xlab = "Time (Hours)", sub = "Afbeelding 3: verandering dichtheid van DR per uur richting equilibrium")
plot(out[, "DR_N"], type = "l", main = "Change in MPL receptor complex in the nucleus per hour", ylab = "Volume DR(N) (fmol / mg protein)", xlab = "Time (Hours)", sub = "Afbeelding 4: verandering volume MPL complex in de celkern per uur richting equilibrium")

```


[4] Simuleer het model voor een duur van twee dagen. Let op: de snelheidsconstante is per uur, dus de tijdsframe moet     hier rekening mee houden


[5] Beschrijf de resultaten: Wat gebeurt er in de tijd en hoe kun je dit verklaren aan de hand van de interacties        tussen de variabelen? Welke veranderingen treden op in (a) de concentratie vrije receptor, (b) de concentratie       cytosol MPL-receptor complex, (c) de concentratie geactiveerd MPL-receptor complex, de totale concentratie receptoren   en de concentratie receptor mRNA. 

Per variable gebeurt het volgende:
mRNA, begint stabiel op 4.74 (fmol / g lever), zodra DR(N) aanwezig is, wordt mRNA zo geinhibeerd dat deze zakt naar ~1.6. Hierna begint mRNA weer aangemaakt te worden naar een equilibrium van ~ 2.7. Omdat er minder mRNA aangemaakt wordt, zullen de R, DR en uiteindelijk ook DR_N gehaltes ook afnemen, waardoor mRNA uiteindelijk minder geinhibeerd wordt, en dus bij T = 8 uur weer toe begint te nemen.

R, er is een constante daling zichtbaar bij de hoeveelheid receptor R, omdat medicijn zich bindt aan de receptors, zal de concentratie vrije receptors R dalen. Waarom neemt de daling van R niet significant af op het moment dat mRNA weer aangemaakt wordt (T=8)? Dit komt waarschijnlijk omdat D nog steeds aanwezig is als constante, en dus continue zal binden aan R. Een andere factor die invloed heeft op R, namelijk DR(N) die weer in R wordt omgezet, zal ook dalen, waardoor er netto een daling van R blijft optreden. Dit verklaart ook waarom het geen liniaire daling is in de grafiek.

DR en DR(N) zijn vergelijkbare grafieken. Het verschil zit in de snelheid van translocatie naar de celkern. Daarom lijkt de DR(N) grafiek 1 tijdseenheid 'achter te lopen'. Er is een hoge hoeveelheid R receptor aanwezig in de startsituatie, dus DR en DR(N) kunnen naar hun startwaarden stijgen (53.4). Daarna begint de concentratie receptor R af te nemen, waardoor er minder medicijn kan binden en de concentraties DR en DR(N) afnemen tot een equilibrium bereikt wordt.


Bepaal welke variabele het belangrijkst is voor de werking van het geneesmiddel en beschrijf deze in detail.

Vanuit het model zou je kunnen zeggen dat de hoeveelheid D, medicijn, en de hoeveelheid vrije receptor R, het meeste invloed hebben op de simulatie. Meer medicijn zorgt ervoor dat het verschil tussen R en DR in het begin groter is, er is dus een hevigere piek in de beginsituatie. Als er minder vrije receptor R aanwezig was, zal sneller het equilibrium bereikt worden, omdat het equilibrium uitgaat van hoeveel DR(N) weer omgezet wordt in R + hoeveel R aangemaakt wordt door mRNA, als er dus geen vrije receptoren aanwezig zijn, is equilibrium vrijwel direct behaald.

Om dit aan te tonen, voeren we de simulatie weer uit en wijzigen we de startwaardes van de variabelen vrije receptor R. Omdat we alleen variabelen testen, wijzigen we niet constante D, we testen wel de andere variablen mRNA, DR en DR(N) in een aparte simulatie.

R = 0 resultaat:
mRNA keldert direct, er geen sterke piek van DR en DR(N) zichtbaar. Dit komt overeen met de verwachting, omdat er geen vrije receptoren aanwezig zijn in de beginsituatie, kan D alleen binden aan de vrije receptoren die door het mRNA en door DR(N) aangemaakt worden, er is dus een zwakke reactie en equilibrium is snel bereikt bij T=10-14

R = 2000:
Bij een erg hoge R concentratie loopt de simulatie vrij gelijk in de vorm van de grafieken, de waardes stijgen wel enorm hoog, dit komt omdat D kan blijven binden aan de hoeveelheid R totdat deze beginconcentratie op is.

mRNAR = 0:
Dit heeft erg weinig effect op de simulatie, dit komt waarschijnlijk door het onderdeel kd_Rm * mRNAR in de formule van mRNAR, omdat mRNAR nu 0 is, zal deze enorm gaan groeien. Hij bereikt in ongeveer T=3 weer de beginwaarde van ~4, hierna loopt de simulatie ongeveer hetzelfde als met de originele beginwaarde van 4.74.

mRNAR = 50:
Andersom zorgt dit deel van de formule er ook voor dat als er erg veel mRNAR is, dit ook erg snel afgebroken wordt. In deze simulatie wordt ook in ongeveer T=3 weer de beginwaarde van ~4 bereikt en loopt de simulatie hetzelfde als met de originele beginwaarden.

DR & DR(N) = 1000:
Bij erg hoge waardes van DR / DR(N) wordt weinig invloed op het mRNA uitgevoerd. Zoals verwacht zal veel DR(N) naar R omgezet worden en neemt R toe, dit leidt echter snel tot een sterke afname van zowel DR, DR(N) en R door de verhoogde inhibitie, en de simulatie lijkt al snel op de originele, equilibrium wordt iets later bereikt, ongeveer 10 uur verschil.

